#include <VirtualWire.h>
#include "structs.h"
#include "packet.h"
#include <ArduinoJson.h>
#include <SoftwareSerial.h>

DeviceId a;
const int RECEIVER_DATA_PIN = 12;
const int TRANSMITTER_DATA_PIN = 8;
const int INTERRUPT_INPUT_PIN = 2;
const int INTERRUPT_OUTPUT_PIN = 3;
const int ledPin = 13;
const int MAX_BUFFER_LEN = 100;
SoftwareSerial port(4, 5);
StaticJsonBuffer<200> jsonBuffer;

int myId, masterId;
uint8_t* masterIdBytes;
int role; // 0 = master, 1 = slave

const int MASTER_ROLE = 0;
const int SLAVE_ROLE = 1;
void setup()
{
  vw_set_ptt_inverted(true); // Required for DR3100
  vw_set_rx_pin(RECEIVER_DATA_PIN);
  vw_set_tx_pin(TRANSMITTER_DATA_PIN);
  
  port.begin(4800);
  port.flush();
  
  vw_setup(2000); // automatically uses PIN 10 
  pinMode(13, OUTPUT);
  Serial.begin(4800);
  pinMode(INTERRUPT_INPUT_PIN, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(INTERRUPT_INPUT_PIN), readAndSendMessage, CHANGE);

  vw_rx_start(); // Start the receiver PLL running

  randomSeed(42);

 
}


void loop()
{ 
  JsonObject* root;
  Serial.println("Trying to read from Serial");
  delay(500);
  do {
    String initializationString = readSerialMessage();
    Serial.println(initializationString);
    root = &jsonBuffer.parseObject(initializationString);
    if(!(*root).success())
    Serial.println("Not Possible to convert");
  else 
    Serial.println("Possible to convert");
  } while (!(*root).success());
  JsonObject& json = *root;
  
  Serial.println("Initialized");
  myId = json["myId"];
  masterId = json["masterId"];
  role = json["role"];
  uint8_t receiverId [2] = {masterId, (masterId >> 8)};
  masterIdBytes = receiverId;
  Serial.println("My id: " + String(myId));
  Serial.println("Master id: " + String(masterId));
  Serial.println("Role: " + String(role));
  transceiverRoutine();
  delay(1000);
  exit(0);

}

void transceiverRoutine() {
  if (role == SLAVE_ROLE) {
    announceToMaster();
  }

}

void randomTimeout(int step) {
  if (step == 0) {
    return random(200, 500);
  } else if (step == 1) {
    return random(500, 1500);
  } else {
    return random(1500, 1000);
  }
}

void announceToMaster() {
  JsonObject& root  = jsonBuffer.createObject();
  root["slaveId"] = myId;
  root["masterId"] = masterId;
  sendMessage(root, masterIdBytes);
 

}

String readSerialMessage() {
  String Data = "";
  boolean characterRead = false;
  while (port.available() || !characterRead)
    {
        
        char character = port.read(); // Receive a single character from the software serial port
        Data.concat(character); // Add the received character to the receive buffer
        characterRead = true;
        if (character == '\n')
        {
            Serial.print("Received: ");
            Serial.println(Data);
            // Add your code to parse the received line here....
            // Clear receive buffer so we're ready to receive the next line
            delay(1000);
            digitalWrite(13,0);
            return Data;
        }
    }
  return Data;
}

void readAndSendMessage() {

  String toSend = readSerialMessage();
  JsonObject& root = jsonBuffer.parseObject(toSend);
  if (!root.success()) {
    Serial.print("Error decoding jsonString");
  }
  sendMessage(root, masterIdBytes);
  
}

Message receiveMessage(long maxTimeoutMillis) {
  uint8_t buf[VW_MAX_MESSAGE_LEN];
  uint8_t buflen = VW_MAX_MESSAGE_LEN;
  int now = millis();
  while (true && millis - now < maxTimeoutMillis) {
    if (vw_get_message(buf, &buflen)) // Non-blocking
    {
      Message message;
      message.buf = *buf;
      message.buflen = buflen;
      return message;
    }
  }
}

int freeRam () 
{
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

boolean sendMessage(JsonObject& messageToSend, uint8_t* receiver) {
  int contentLength = messageToSend["length"];
  char* content = messageToSend["content"];
  uint8_t buf [contentLength + 3];
  buf[0] = receiver[0];
  buf[1] = receiver[1];

  for (int i = 0; i < contentLength; i++) {
    buf[i + 3] = (uint8_t)content[i];
  }
  buf[contentLength + 2] = '\0';
  vw_send((uint8_t *)buf, contentLength + 3);
  vw_wait_tx();
}

