#include <VirtualWire.h>
#include "packet.h"
#define ACK_POS 0
#define MIRROR_POS 1
#define HEADER_LENGTH 1 // 8 bits
#define ID_LENGTH 2 // 16 bits
#define CONTROL_CHARACTER_LENGTH 1
#define STX 0x02
#define ETX 0x03

Packet craftPacket(DeviceId senderId, DeviceId* recipients, int numberOfRecipients, uint8_t* content, int contentLength, boolean isAck) {
  uint8_t header = 0;
  if (isAck) {
    header = set_bit(header, ACK_POS, 1);
  }
  if (numberOfRecipients > 1) {
    header = set_bit(header, MIRROR_POS, 1);
  }

  int packetLength = HEADER_LENGTH + ID_LENGTH + numberOfRecipients * ID_LENGTH + CONTROL_CHARACTER_LENGTH + contentLength + CONTROL_CHARACTER_LENGTH;
  uint8_t packet[packetLength];
  packet[0] = header;
  packet[1] = senderId.id[0];
  packet[2] = senderId.id[1];
  int counter = 2;
  for (int i = 0; i < numberOfRecipients; i++) {
    packet [3 + i] = recipients[i].id[0];
    packet [4 + i] = recipients[i].id[1];
    counter += 2;
  }
  counter++;
  packet[counter] = STX;
  counter++;
  for (int i = 0; i < contentLength; i++) {
    packet[i+counter] = content[i];
  }
  packet[packetLength-1] = ETX;
}

uint8_t get_bit(uint64_t bits, uint8_t pos)
{
  return (bits >> pos) & 0x01;
}

uint8_t set_bit(uint8_t bits, uint8_t pos, uint8_t value)
{
  uint8_t mask = 1LL << (7 - pos);
  if (value)
    bits |= mask;
  else
    bits &= ~mask;
  return bits;
}
