
typedef struct deviceId{
    uint8_t id [2];
} DeviceId;

typedef struct packet{
  // HEADER FIRST
  uint8_t header;
  DeviceId senderId;
  DeviceId* recipients;
  uint8_t content;
  } Packet;
  

Packet craftPacket(DeviceId senderId, DeviceId* recipients, int numberOfRecipients, uint8_t* content, int contentLength, boolean isAck);
uint8_t set_bit(uint8_t bits, uint8_t pos, uint8_t value);
uint8_t get_bit(uint64_t bits, uint8_t pos);
